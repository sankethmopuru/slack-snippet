package controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;
import responsehandler.MessageActionResponseHandler;
import responsehandler.ResponseHandler;
import responsehandler.factory.ResponseHandlerFactory;
import service.SlackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/slack/")
public class SlackController {
    private static final Logger LOG = LogManager.getLogger(SlackController.class);

    @Autowired
    private ResponseHandlerFactory responseHandlerFactory;

    @Autowired
    private SlackService slackService;

    @Autowired
    private MessageActionResponseHandler messageActionResponseHandler;

    /**
     * Sample slack event json
     *
     * {
     *     "token": "XXYYZZ",
     *     "team_id": "TXXXXXXXX",
     *     "api_app_id": "AXXXXXXXXX",
     *     "event": {
     *         "type": "resources_added",
     *         "resources": [
     *             {
     *                 "resource": {
     *                     "type": "im",
     *                     "grant": {
     *                         "type": "specific",
     *                         "resource_id": "DXXXXXXXX"
     *                     }
     *                 },
     *                 "scopes": [
     *                     "chat:write:user",
     *                     "im:read",
     *                     "im:history",
     *                     "commands"
     *                 ]
     *             }
     *         ]
     *     },
     *     "type": "event_callback",
     *     "authed_teams": [],
     *     "event_id": "EvXXXXXXXX",
     *     "event_time": 1234567890
     * }
     * @param eventJson JSON String provided in the request body
     */
    @PostMapping(value = "/actor", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public void handleSlackEvent(@RequestBody String eventJson, HttpServletRequest request, HttpServletResponse response) {
        LOG.info("Received slack event " + eventJson);

        JSONObject jsonObject = new JSONObject(eventJson);
        String eventType = jsonObject.getString("type");

        ResponseHandler responseHandler = responseHandlerFactory.getResponseHandler(eventType);
        responseHandler.handleResponse(jsonObject, request, response);
    }

    @PostMapping(value = "/messageAction", consumes = "application/x-www-form-urlencoded")
    public void handleMessageAction(@RequestParam("payload") String eventJson, HttpServletRequest request, HttpServletResponse response) {
        LOG.info("Received slack event " + eventJson);

        JSONObject jsonObject = new JSONObject(eventJson);
        messageActionResponseHandler.handleResponse(jsonObject, request, response);
    }

    @GetMapping(value = "/send", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public String sendMessage(@RequestParam("message") String message, @RequestParam("channel") String channelName) {
        return slackService.sendMessage(message, channelName);
    }
}
