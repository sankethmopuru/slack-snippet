package responsehandler;

import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ResponseHandler {
    void handleResponse(JSONObject object, HttpServletRequest request, HttpServletResponse response);
}
