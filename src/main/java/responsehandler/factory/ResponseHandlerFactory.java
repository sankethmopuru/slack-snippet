package responsehandler.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import responsehandler.ChallengeResponseHandler;
import responsehandler.EventCallbackResponseHandler;
import responsehandler.MessageActionResponseHandler;
import responsehandler.ResponseHandler;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Factory class to construct ResponseHandlers
 */
@Component
public class ResponseHandlerFactory {

    private Map<String, ResponseHandler> responseHandlerMap;

    @Autowired
    private ChallengeResponseHandler challengeResponseHandler;

    @Autowired
    private EventCallbackResponseHandler eventCallbackResponseHandler;

    @PostConstruct
    public void init() {
        responseHandlerMap = new ConcurrentHashMap<>();
        responseHandlerMap.put("url_verification", challengeResponseHandler);
        responseHandlerMap.put("event_callback", eventCallbackResponseHandler);
    }

    /**
     * Return an instance of {@link ResponseHandler} for the particular event type
     */
    public ResponseHandler getResponseHandler(String eventType) {
        ResponseHandler responseHandler = responseHandlerMap.get(eventType);

        if (responseHandler == null) {
            throw new IllegalArgumentException("No response handler configured for event type " + eventType);
        }

        return responseHandler;
    }
}
