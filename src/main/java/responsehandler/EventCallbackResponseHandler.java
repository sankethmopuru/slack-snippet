package responsehandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Handler for an event of type event_callback
 *
 * {
 *     "token": "one-long-verification-token",
 *     "team_id": "T061EG9R6",
 *     "api_app_id": "A0PNCHHK2",
 *     "event": {
 *         "type": "message",
 *         "channel": "C024BE91L",
 *         "user": "U2147483697",
 *         "text": "Live long and prospect.",
 *         "ts": "1355517523.000005",
 *         "event_ts": "1355517523.000005",
 *         "channel_type": "channel"
 *     },
 *     "type": "event_callback",
 *     "authed_teams": [
 *         "T061EG9R6"
 *     ],
 *     "event_id": "Ev0PV52K21",
 *     "event_time": 1355517523
 * }
 */
@Component
public class EventCallbackResponseHandler implements ResponseHandler {
    private static final Logger LOG = LogManager.getLogger(EventCallbackResponseHandler.class);

    private static ThreadLocal<HashMap<Integer, Double>> measurements =
            ThreadLocal.withInitial(HashMap::new);

    @Override
    public void handleResponse(JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        JSONObject event = object.getJSONObject("event");

        if (event == null) {
            throw new IllegalArgumentException("Received event callback without event object");
        }

        LOG.info("Received event: " + event.toString());

        String eventType = event.getString("type");

        if (eventType.equals("message")) {
            LOG.info("Received message: " + event.getString("text"));
        }

        response.setStatus(HttpStatus.OK.value());
    }
}
