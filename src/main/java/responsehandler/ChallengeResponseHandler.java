package responsehandler;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Response handler for app verification. Should be called one once when app is being
 * verified by slack
 */
@Component
public class ChallengeResponseHandler implements ResponseHandler {

    @Override
    public void handleResponse(JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        String challenge = object.getString("challenge");

        response.setHeader(HttpHeaders.CONTENT_TYPE, MimeTypeUtils.TEXT_PLAIN_VALUE);

        try (PrintWriter writer = response.getWriter()) {
            writer.print(challenge);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
