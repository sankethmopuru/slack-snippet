package responsehandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * {
 *   "token": "Nj2rfC2hU8mAfgaJLemZgO7H",
 *   "callback_id": "chirp_message",
 *   "type": "message_action",
 *   "trigger_id": "13345224609.8534564800.6f8ab1f53e13d0cd15f96106292d5536",
 *   "response_url": "https://hooks.slack.com/app-actions/T0MJR11A4/21974584944/yk1S9ndf35Q1flupVG5JbpM6",
 *   "team": {
 *     "id": "T0MJRM1A7",
 *     "domain": "pandamonium",
 *   },
 *   "channel": {
 *     "id": "D0LFFBKLZ",
 *     "name": "cats"
 *   },
 *   "user": {
 *     "id": "U0D15K92L",
 *     "name": "dr_maomao"
 *   },
 *   "message": {
 *     "type": "message",
 *     "user": "U0MJRG1AL",
 *     "ts": "1516229207.000133",
 *     "text": "World's smallest big cat! <https://youtube.com/watch?v=W86cTIoMv2U>"
 *   }
 * }
 */
@Component
public class MessageActionResponseHandler implements ResponseHandler{
    private static final Logger LOG = LogManager.getLogger(MessageActionResponseHandler.class);

    @Override
    public void handleResponse(JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        JSONObject message = object.getJSONObject("message");

        if (message == null) {
            throw new IllegalArgumentException("Received message action event without message payload");
        }

        String text = message.getString("text");
        String callbackId = object.getString("callback_id");
        LOG.info("Received message via callback: " + callbackId + ". Message: " + text);
        response.setStatus(HttpStatus.OK.value());
    }
}
