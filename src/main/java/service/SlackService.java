package service;

/**
 * Service to send messages to slack channels
 */
public interface SlackService {
    String sendMessage(String message, String channelName);
}
