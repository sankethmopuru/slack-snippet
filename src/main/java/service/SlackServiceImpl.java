package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

/**
 *
 */
@Component
public class SlackServiceImpl implements SlackService{
    private static final Logger LOG = LogManager.getLogger(SlackServiceImpl.class);
    private static final String API_METHOD = "chat.postMessage";

    @Value("${slack.token}")
    private String token;

    @Value("${slack.apiBase}")
    private String apiBase;

    @Override
    public String sendMessage(String message, String channelName) {
        LOG.info("Trying to post message: '" + message + "' to channel: " + channelName);

        HttpsURLConnection connection;
        DataOutputStream stream = null;
        try {
            URL url = new URL(apiBase + API_METHOD);
            connection = (HttpsURLConnection) url.openConnection();

            connection.setRequestMethod(RequestMethod.POST.toString());
            connection.setRequestProperty(HttpHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON_VALUE + ";charset=utf-8");
            connection.setRequestProperty(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            connection.setDoOutput(true);
            connection.setDoInput(true);

            String payload = constructPayload(message, channelName);
            LOG.info("Sending payload: " + payload);

            stream = new DataOutputStream(connection.getOutputStream());
            stream.writeBytes(payload);
            stream.close();

            BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = inputStream.readLine()) != null) {
                content.append(inputLine);
            }
            inputStream.close();

            LOG.info("Received response from slack: " + content.toString());
            return content.toString();
        } catch (IOException e) {
            LOG.error("Failed to send message to slack", e);
            throw new RuntimeException("Failed to send message to slack", e);
        }
    }

    private String constructPayload(String message, String channelName) {
        StringBuilder sb = new StringBuilder();
        return new JSONObject()
                .put("channel", channelName)
                .put("text", message).toString();
    }
}
