package model;

import java.util.List;

public class SlackEventWrapper {
    private String token;
    private String challenge;
    private String type;
    private String teamId;
    private String apiAppId;
    private String eventId;
    private Integer eventTime;
    private List<String> authedUsers;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
